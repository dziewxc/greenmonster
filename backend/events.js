let events = [];

events.push({
  name: "feature task",
  listeners: [
    {
      name: "commands/featureTaskCommand"
    }
  ]
});

events.push({
  name: "complete task",
  listeners: [
    {
      name: "commands/completeTaskCommand"
    }
  ]
});

module.exports = events;