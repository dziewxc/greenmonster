const routes = [
  {
    route: '/api/tasks',
    path: './controllers/api/TaskController'
  },
  {
    route: '/api/badges',
    path: './controllers/api/BadgeController'
  },
  {
    route: '/api/users',
    path: './controllers/api/UserController'
  },
  {
    route: '/api/groups',
    path: './controllers/api/GroupController'
  }
];
module.exports = routes;