'use strict';
module.exports = function(sequelize, DataTypes) {
  let Comment = sequelize.define('Comment', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    content: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {
    underscored: true,
    classMethods: {
      associate: function(models) {
        Comment.belongsTo(models.User, {
          foreignKey: 'user_id',
          onDelete: 'SET NULL',
        });
        Comment.belongsTo(models.Task, {
          foreignKey: 'task_id',
          onDelete: 'SET NULL',
        });
      }
    }
  });
  return Comment;
};