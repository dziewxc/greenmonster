'use strict';
module.exports = function(sequelize, DataTypes) {
  let Image = sequelize.define('Image', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    path: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        max: 500
      }
    },
  }, {
    underscored: true,
    classMethods: {
      associate: function(models) {
        Image.hasMany(models.Task, {
          foreignKey: 'image_id'
        });
        Image.hasMany(models.Badge, {
          foreignKey: 'image_id'
        });
      }
    }
  });
  return Image;
};