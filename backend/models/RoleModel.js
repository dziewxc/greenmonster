'use strict';
module.exports = function(sequelize, DataTypes) {
  let Role = sequelize.define('Role', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {
    underscored: true,
    timestamps: false,
    classMethods: {
      associate: function(models) {
        Role.hasMany(models.User, {
          foreignKey: 'role_id'
        });
      }
    }
  });
  return Role;
};
