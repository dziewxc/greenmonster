'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('TaskUser', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isIn: [["completed", "chosen", "hold", "featured", "timeEnded"]] //TODO: contants
      }
    }
  }, {
    tableName: 'task_user',
    underscored: true
  });
};