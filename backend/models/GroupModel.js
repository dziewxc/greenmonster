'use strict';
module.exports = function(sequelize, DataTypes) {
  let Group = sequelize.define('Group', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  }, {
    underscored: true,
    classMethods: {
      associate: function(models) {
        Group.belongsTo(models.User, {
          foreignKey: 'admin_id',
          onDelete: 'SET NULL',
        });
        Group.belongsToMany(models.User, {through: 'GroupUser', foreignKey: 'group_id', onDelete: 'CASCADE'});
      }
    }
  });
  return Group;
};