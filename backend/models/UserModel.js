'use strict';
module.exports = function(sequelize, DataTypes) {
  let User = sequelize.define('User', {

    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: true
      },
      unique: {
        args: true,
        msg: 'Oops. Looks like you already have an account with this email address. Please try to login.',
        fields: [sequelize.fn('lower', sequelize.col('email'))]
      },
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        max: 100
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    blocked: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
  }, {
    underscored: true,
    classMethods: {
      associate: function(models) {
        User.belongsTo(models.Role, {
          foreignKey: 'role_id',
          onDelete: 'SET NULL',
        });
        User.hasMany(models.Group, {
          foreignKey: 'admin_id'
        });
        User.hasMany(models.TaskDescriptionHistory, {
          foreignKey: 'moderator_id',
          as: 'tasksDescriptionApproved'
        });
        User.hasMany(models.TaskDescriptionHistory, {
          foreignKey: 'user_id',
          as: 'tasksDescriptionAdded'
        });
        User.belongsToMany(models.Group, {through: 'GroupUser', foreignKey: 'user_id', onDelete: 'SET NULL'});
        User.belongsToMany(models.Badge, {through: 'badge_user', foreignKey: 'user_id'});
        User.belongsToMany(models.Task, {through: 'TaskUser', foreignKey: 'user_id', onDelete: 'SET NULL'});
      }
    }
  });
  return User;
};