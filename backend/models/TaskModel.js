'use strict';
module.exports = function(sequelize, DataTypes) {
  let Task = sequelize.define('Task', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    shortDescription: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'short_description'
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      isIn: [["rejected", "waiting", "approved"]],
      defaultValue: "waiting",
    },
  }, {
    underscored: true,
    classMethods: {
      associate: function(models) {
        Task.belongsToMany(models.Category, {through: 'task_category', foreignKey: 'task_id'});
        Task.belongsTo(models.Image, {
          foreignKey: 'image_id',
          onDelete: 'SET NULL',
        });
        Task.hasMany(models.Comment, {
          foreignKey: 'task_id'
        });
        Task.belongsToMany(models.User, {through: 'TaskUser', foreignKey: 'task_id'});
      },
    }
  });
  return Task;
};