'use strict';
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('GroupUser', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    acceptedByUser: {
      type: DataTypes.STRING,
      defaultValue: "waiting",
      allowNull: false,
      validate: {
        isIn: [["rejected", "waiting", "approved"]]
      },
      field: 'accepted_by_user'
    },
    acceptedByAdmin: {
      type: DataTypes.STRING,
      defaultValue: "waiting",
      allowNull: false,
      validate: {
        isIn: [["rejected", "waiting", "approved"]]
      },
      field: 'accepted_by_admin'
    },
  }, {
    tableName: 'group_user',
    underscored: true
  });
};
