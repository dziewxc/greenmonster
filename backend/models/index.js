'use strict';

const fs        = require('fs');
const path      = require('path');
const Sequelize = require('sequelize');
const basename  = path.basename(module.filename);//index.js
const env       = process.env.NODE_ENV || 'dev';
const config    = require('../config/config.json').database;
const db        = {};

let sequelize = new Sequelize(config[env].database, config[env].username, config[env].password, config);

fs
  .readdirSync(__dirname)
  .filter((file) => { //get all js files except current file
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach((file) => {
    let model = sequelize['import'](path.join(__dirname, file)); //.import?
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
