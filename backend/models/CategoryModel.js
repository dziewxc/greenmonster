'use strict';
module.exports = function(sequelize, DataTypes) {
  let Category = sequelize.define('Category', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: 'compositeIndex'
    },
  }, {
    underscored: true,
    classMethods: {
      associate: function(models) {
        Category.belongsToMany(models.Task, {through: 'task_category', foreignKey: 'category_id'});
      }
    }
  });
  return Category;
};
