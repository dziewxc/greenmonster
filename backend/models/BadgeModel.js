'use strict';
module.exports = function(sequelize, DataTypes) {
  let Badge = sequelize.define('Badge', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        max: 100
      }
    },
    taskAmount: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'task_amount'
    },
    categoryAmount: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'category_amount'
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        max: 500
      }
    },
  }, {
    underscored: true,
    classMethods: {
      associate: function(models) {
        Badge.belongsTo(models.Image, {
          foreignKey: 'image_id',
          onDelete: 'SET NULL',
        });
        Badge.belongsTo(models.Task, {
          foreignKey: 'task_id',
          onDelete: 'SET NULL',
          allowNull: true,
        });
        Badge.belongsTo(models.Category, {
          foreignKey: 'category_id',
          onDelete: 'SET NULL',
          allowNull: true,
        });
        Badge.belongsToMany(models.User, {through: 'badge_user', foreignKey: 'badge_id'});
      }
    }
  });
  return Badge;
};