'use strict';
module.exports = function(sequelize, DataTypes) {
  let TaskDescriptionHistory = sequelize.define('TaskDescriptionHistory', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    status: {
      type: DataTypes.INTEGER,
      isIn: [[0, 1, 2]],
      defaultValue: 0,
      allowNull: false,
    }
  }, {
    tableName: 'task_description_history',
    underscored: true,
    classMethods: {
      associate: function(models) {
        TaskDescriptionHistory.belongsTo(models.User, {
          foreignKey: 'user_id',
          onDelete: 'SET NULL'
        });
        TaskDescriptionHistory.belongsTo(models.User, {
          foreignKey: 'moderator_id',
          onDelete: 'SET NULL'
        });
      }
    }
  });
  return TaskDescriptionHistory;
};