const CommandBus = require('./CommandBus');
const models = require('../models');
const HttpError = require('../helpers/errorHelper').HttpError;
const Sequelize = require('sequelize');

let addActionForTask = (userId, taskId, actionType) => {
  return models.User.findOne({where: {id: userId}})
    .then((user) => {
      return new Promise((resolve, reject) => {
        if (!user) {
          return reject(new HttpError({status: 400, message: "There is no user with this id."}));
        }
        resolve(user);
      });
    }, () => {
      return new HttpError({status: 500, message: "Something went wrong."})
    })
    .then(() => {
      return models.Task.findOne({where: {id: taskId, status: "approved"}})
        .then((task) => {
          return new Promise((resolve, reject) => {
            if (!task) {
              return reject(new HttpError({status: 400, message: "There is no task with this id."}));
            }
            resolve(task);
          });
        }, () => {
          return new HttpError({status: 500, message: "Something went wrong."})
        })
    })
    .then(() => {
      return validateTaskActionType(actionType, userId, taskId);
    })
  /*    .then(() => {

   });*/
};
//todo: if complete -> remove featured
//todo: if return -> return 2 featured and 1 new

let validateTaskActionType = (actionType, userId, taskId) => {
  let actionTypes = ["complete", "return", "choose", "hold", "timeEnded", "unhold"]; //TODO Set
  return new Promise((resolve, reject) => {
    if (actionTypes.indexOf(actionType) === -1) {
      return reject({status: 400, message: "Action not valid."});
    }
    models.TaskUser.findAll({limit: 1, order: [['id', 'DESC']], where: {user_id: userId, task_id: taskId}})
      .then((taskUser) => {
        taskUser = taskUser[0];
        if (!taskUser) {
          if ((["choose", "hold"].indexOf(actionType) !== -1)) {
            return resolve(taskUser);
          }
          return reject({status: 400, message: "Task not chosen yet."});
        } else {
          if (!validateTaskActionFlow(actionType, taskUser.status)) {
            return reject({status: 400, message: "Task not allowed here."});
          }
          //user cant change status for another than "return" or "complete" if there is chosen or completed for today
          //user cant complete/ task that were chosen 24h prior to now
          //user shouldn't be able to hold more than 3 tasks
          //TODO: rest of validation
          resolve(taskUser);
        }
      });
  });
};

let validateTaskActionFlow = (newAction, lastActionStatus) => {
  switch (newAction) {
    case "complete":
      return ["chosen"].indexOf(lastActionStatus) !== -1;
      break;
    case "return":
      return ["chosen"].indexOf(lastActionStatus) !== -1;
      break;
    case "choose":
      return ["hold", "featured"].indexOf(lastActionStatus) !== -1;
      break;
    case "hold":
      return ["featured"].indexOf(lastActionStatus) !== -1;
      break;
    case "timeEnded":
      return ["chosen"].indexOf(lastActionStatus) !== -1;
      break;
  }
};

let getTasksForToday = (userId) => {
  //TODO: ongoing canceling/ignoring discussion on how to break promise
  //TODO: if featured, return featured
  return checkForTodayTasks(userId)
    .then((tasks) => {
      if (tasks.length > 0) {
        return new Promise((resolve) => {
          return resolve(tasks);
        });
      } else {
        return checkForHold(userId)
          .then((hold) => {
            if (hold.length > 2) {
              return new Promise((resolve) => {
                return resolve(hold);
              });
            } else {
              let taskNumberToGet = 3 - hold.length;
              let holdTasks = new Map(hold);
              let randomTasks = new Map(getRandomTasks(userId, taskNumberToGet));
              return new Promise((resolve) => {
                return resolve(new Map([...holdTasks, ...randomTasks])); //TODO:: not sure what ... means here
              });
            }
          });
      }
    });
};

//looking for completed, chosen or already featured
let checkForTodayTasks = (userId) => {//get completed today or hold/featured in general
  return models.TaskUser.findAll({
    where: {
      $or: [
        Sequelize.and(
          Sequelize.where(
            Sequelize.fn('DATE', Sequelize.col('created_at')),
            Sequelize.literal('CURRENT_DATE')
          ),
          { user_id: userId },
          { status: "completed" }
        ),
        Sequelize.and(
          { user_id: userId },
          Sequelize.or(
            { status: "featured" },
            { status: "chosen" }
          )
        )
      ]
    }
  }).then((tasks) => {
    return filterTodayTasks(tasks);
  });
};

let filterTodayTasks = (tasks) => {
  //if there is completed, just return it, coz it should not be featured/chosen in this case
  let completedTask = tasks.filter((element) => {
    return element.status === "completed";
  });
  if (completedTask.length > 0) {
    return new Promise((resolve) => {
      return resolve(completedTask);
    });
  }
  let promises = [];
  //for chosen and featured, check date and endTime if needed
  tasks.forEach((task) => {
    if (checkIfToday(task.created_at)) {
      promises.push(endTimeForTask(task)); //it should not push deleted
      tasks = tasks.filter((element) => {
        return element.id !== task.id; //TODO przetestowac to po rozwiazaniu timezonow
      });
    }
  });
  return Promise.all(promises).then(() => {
    return new Promise((resolve) => {
      return resolve(tasks);
    });
  });
};

let checkIfToday = (date) => { //TODO: rewrite
  let now = new Date();
  let utcYear = now.getUTCFullYear();
  let utcDay = now.getUTCDate();
  let utcMonth = now.getUTCMonth();
  let nowInUTC = new Date(utcYear, utcMonth, utcDay);

  return date.toDateString() !== nowInUTC.toDateString();
};

let endTimeForTask = (task) => {
  //TODO: here it should emit event
  return task.destroy().then();
};

let checkForHold = (userId) => {
  return models.TaskUser.findAll({where: {user_id: userId, status: "hold"}});
};

let getRandomTasks = (userId, taskNumberToGet) => {
  //should return array
};

module.exports = {
  addActionForTask,
  getTasksForToday
};