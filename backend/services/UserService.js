const jwt = require('jwt-simple');
const secretKey = require('../config/config').jwtsecret;
const models  = require('../models');
const bcryptPromise = require('../helpers/promiseHelper');
const HttpError = require('../helpers/errorHelper').HttpError;

let getToken = function(email, password) {
  //TODO:check if email and password exist
  return models.User.findOne({ where: {email: email} })
    .then(function(user) {
    return new Promise((resolve, reject) => {
      if(user) {
        bcryptPromise.bcryptCompare(password, user.password)
          .then(function () {//TODO:roles to token
          let token = jwt.encode({id: user.id, email}, secretKey);
          resolve(token);
        }, function (error) {
          reject(error);
        });
      } else {
        reject(new HttpError(400, "There is no account with this email address."))
      }
    });
  });
};

let createUser = function(username, password, email) {
  return validateUserData(username, password, email)
    .then(function() {
      return bcryptPromise.bcryptHash(password)
    })
    .then(function(hash) {
      return models.User.build({username, email, password: hash}).save();
    })
};

let checkIfEmailUnique = function(email) {
  return models.User.findAll({
    where: {
      email: email
    }
  }).then(function(results) {
    return new Promise(function (resolve, reject) {
      if (results.length > 0) {
        reject(new HttpError(400, "There is account with this email address."));
      } else {
        resolve(true)
      }
    })
  })
};

let validateUserData = function(username, password, email) {
  return new Promise(function(resolve, reject) {
    if(!username || !password || !email) {
      reject(new HttpError(400, "You have to provide username, password and email."));
    } else {
      resolve(checkIfEmailUnique(email));
    }
  });
};

let checkIfUserExists = function(user) {
  return models.User.findOne({where: {id: user}})
    .then(function(user) {
      return new Promise(function (resolve, reject) {
        if(!user) {
          return reject(new HttpError(400, "There is no user with this id."));
        }
        resolve(user);
      });
    }, function() {
      return new Promise(function (resolve, reject) {
        reject(new HttpError(500, "Something went wrong."));
      });
    });
};

let getAllGroupsForUser = function(userId) {
//
};

module.exports = {
  getToken,
  createUser,
  checkIfUserExists
};