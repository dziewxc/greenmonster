const models  = require('../models');
const userService  = require('../services/UserService');
const HttpError = require('../helpers/errorHelper').HttpError;

let createGroup = (adminId, title, users) => {
  let group = models.Group.build({title, admin_id: adminId});
  return group.save()
    .then((group) => {
      if(users && users.length > 0) {
        group.addUsers(users);
        //TODO: check if there are users with those ids
        //dont have to check if those users are already in this group coz its new group
      }
      return group.save();
    });
};

let addUsersToGroup = (adminId, groupId, users) => {
  //check if
};

let getGroupDetails = (groupId) => {
  return models.Group.findOne({where: {id: groupId}})
    .then((group) => {
      return new Promise((resolve, reject) => {
        if (!group) {
          return reject(new HttpError(404, "Group not found."));
        } else {
          resolve(group)
        }
      })
  });
};

let addUserToGroup = (groupId, userToAdd, loggedInUser) => {
  return userService.checkIfUserExists(userToAdd)
  .then(() => {
    return models.GroupUser.findOne({where: {group_id: groupId, user_id: userToAdd}})
      .then((groupUser) => {
        return new Promise((resolve, reject) => {
          if(groupUser) {
            return reject(new HttpError(400, "User already added to group."));
          }
          resolve(groupUser);
        });
      }, () => {
        return new Promise((resolve, reject) => {
          reject(new HttpError(500, "Something went wrong."));
        });
      });
  })
  .then(() => {return checkIfGroupExists(groupId)})
  .then((group) => {
    if(parseInt(userToAdd) === parseInt(loggedInUser)) {
      return models.GroupUser.build({group_id: groupId, user_id: userToAdd, acceptedByUser: 1}).save();
    } else {
      if(group.admin_id === parseInt(loggedInUser)) {
        return models.GroupUser.build({group_id: groupId, user_id: userToAdd, acceptedByAdmin: 1}).save();
      } else {
        return new Promise((resolve, reject) => {
          reject(new HttpError(403, "You don't have permissions."));
        });
      }
    }
  });
};

let changeUserStatusInGroup = (groupId, groupMember, loggedInUser, status) => {
  return userService.checkIfUserExists(groupMember)
  .then(() => {return checkIfGroupExists(groupId)})
  .then((group) => {
    return models.GroupUser.findOne({where: {group_id: groupId, user_id: groupMember}})
      .then((groupUser) => {
        return new Promise((resolve, reject) => {
          if (!groupUser) {
            return reject(new HttpError(400, "User not in group."))
          }
          resolve({groupUser, group});
        });
      },
      () => {
        return new Promise((resolve, reject) => {
          reject(new HttpError(500, "Something went wrong."));
        })
      });
  })
  .then((params) => {
    return new Promise((resolve, reject) => {
      if(parseInt(groupMember) === parseInt(loggedInUser) || params.group.admin_id === parseInt(loggedInUser)) {
        if(parseInt(status) === 1 || parseInt(status) === -1) {
          if(params.group.admin_id === parseInt(loggedInUser)) {
            return resolve(params.groupUser.update({acceptedByAdmin: status}));
          } else {
            return resolve(params.groupUser.update({acceptedByUser: status}));
          }
        } else {
          reject(new HttpError(400, "Wrong status code."));
        }
      } else {
        reject(new HttpError(403, "You don't have permissions."));
      }
    });
  });
};

let checkIfGroupExists = (groupId) => {
  return models.Group.findOne({where: {id: groupId}})
    .then((group) => {
      return new Promise((resolve, reject) => {
        if(!group) {
          return reject(new HttpError(400, "There is no group with this id."));
        }
        resolve(group);
      });
    }, () => {
      return new Promise((resolve, reject) => {
        reject(new HttpError(500, "Something went wrong."));
      });
    });
};

let removeUserFromGroup = (groupId, groupMember, loggedInUser) => {
  return userService.checkIfUserExists(groupMember)
  .then(() => {return checkIfGroupExists(groupId)})
  .then((group) => {
    return models.GroupUser.findOne({where: {group_id: groupId, user_id: groupMember}})
      .then((groupUser) => {
          return new Promise((resolve, reject) => {
            if (!groupUser) {
              return reject(new HttpError(400, "User not in group."))
            }
            resolve({groupUser, group});
          });
        },
        () => {
          return new Promise((resolve, reject) => {
            reject(new HttpError(500, "Something went wrong."));
          })
        });
  })
  .then((params) => {
    return new Promise((resolve, reject) => {
      if(parseInt(groupMember) === parseInt(loggedInUser)) {
        if(params.groupUser.acceptedByAdmin === 0) {
          return resolve(params.groupUser.destroy());
        } else {
          return reject(new HttpError(400, "Invitation or request out of date."))
        }
      }
      if(params.group.admin_id === parseInt(loggedInUser)) {
        if(params.groupUser.acceptedByUser === 0) {
          return resolve(params.groupUser.destroy());
        } else {
          return reject(new HttpError(400, "Invitation or request out of date."))
        }
      }
    });
    })
};

module.exports = {
  createGroup,
  getGroupDetails,
  addUserToGroup,
  changeUserStatusInGroup,
  removeUserFromGroup
};