const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');

let emitter = require('./services/EventsEmitter');
const db = require('./models');
const config = require('./config/config.json');
const routes = require('./routing');
const events = require('./events');
const authHelper = require('./helpers/authHelper');

const app = express();

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(authHelper);

for(let routing of routes) {
  app.use(routing.route, require(routing.path));
}

for(let event of events) {
  emitter.on(event.name, () => {
    for(let listener of event.listeners) {
      let command = require("./" + listener.name);
      command.invoke();
    }
  });
}

emitter.emit('feature task');

db.sequelize.sync()
  .then(console.log("synced"))
  .catch(function (e) {
    throw new Error(e);
  });

app.listen(config.port, function () { //the same as http.Server.listen().
  console.log('App listening on port' + config.port + '!')
});