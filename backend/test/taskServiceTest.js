process.env.NODE_ENV = 'test';

const assert = require('chai').assert;
const models  = require('../models');
const seedHelper  = require('../helpers/seedHelper');
const TaskService  = require('../services/TaskService');


describe("Task Service Test", () => {
  before(function(done) {
    models.sequelize.sync({ force : true })
      .then(() => {
        return seedHelper.testSeed()
      })
      .then(() => {
        done();
      })
  });
  describe('Task action validation', () => {
    it('should return error for no user', () => {
      return TaskService.addActionForTask(15, 3, 'complete')
        .then((result) => {
          assert.equal(1, 2);
        }, (error) => {
          assert.equal("There is no user with this id.", error.message);
        });
    });
    it('should return error for no task', () => {
      return TaskService.addActionForTask(1, 30, 'complete')
        .then((result) => {
          assert.equal(1, 2);
        }, (error) => {
          assert.equal("There is no task with this id.", error.message);
        });
    });
    it('should return error for not approved task.', () => {
      return TaskService.addActionForTask(1, 5, 'complete')
        .then((result) => {
          assert.equal(1, 2);
        }, (error) => {
          assert.equal("There is no task with this id.", error.message);
        });
    });
    it('should return error for rejected task.', () => {
      return TaskService.addActionForTask(1, 4, 'complete')
        .then((result) => {
          assert.equal(1, 2);
        }, (error) => {
          assert.equal("There is no task with this id.", error.message);
        });
    });
    it('should return error for non-existent task action', () => {
      return TaskService.addActionForTask(1, 1, 'test')
        .then((result) => {
          assert.equal(1, 2);
        }, (error) => {
          assert.equal("Action not valid.", error.message);
        });
    });
    it('should return error for user trying complete task that is not chosen by him yet', () => {
      return TaskService.addActionForTask(1, 2, 'complete')
        .then((result) => {
          assert.equal(1, 2);
        }, (error) => {
          assert.equal("Task not chosen yet.", error.message);
        });
    });
    it('should return error for user trying return task that is not chosen by him yet', () => {
      return TaskService.addActionForTask(1, 2, 'return')
        .then((result) => {
          assert.equal(1, 2);
        }, (error) => {
          assert.equal("Task not chosen yet.", error.message);
        });
    });
    it('should return error for timeout when task that is not chosen by user yet', () => {
      return TaskService.addActionForTask(1, 2, 'timeEnded')
        .then((result) => {
          assert.equal(1, 2);
        }, (error) => {
          assert.equal("Task not chosen yet.", error.message);
        });
    });
    it('should return error for user trying unhold task that is was not hold by him', () => {
      return TaskService.addActionForTask(1, 2, 'unhold')
        .then((result) => {
          assert.equal(1, 2);
        }, (error) => {
          assert.equal("Task not chosen yet.", error.message);
        });
    });
  });
});