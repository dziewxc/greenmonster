const router = require('express').Router();
const userService = require('../../services/UserService');
const taskService = require('../../services/TaskService');
const errorHelper = require('../../helpers/errorHelper');
const authorizationHelper = require('../../helpers/authorizationHelper');


router.post('/token', function (request, response) {
  let {email, password} = request.body;
  userService.getToken(email, password).then(function(token) {
    response.json(token)
  }, function(error) {
    errorHelper.send(response, error);
  });
});

router.post('/', function (request, response) {
  let {username, password, email} = request.body;
  userService.createUser(username, password, email).then(function(user) {
    response.json(user)
  }, function(error) {
    errorHelper.send(response, error);
    //TODO: do not send password in user object
  });
});

router.get('/:id/groups', authorizationHelper.isLoggedIn, function (request, response) {
  userService.getAllGroupsForUser(request.auth.id).then(function(groups) {
    response.json(groups)
  }, function(error) {
    errorHelper.send(response, error);
  });
});

router.post('/tasks/:taskId', authorizationHelper.isLoggedIn, function (request, response) {
  let {action} = request.body;
  taskService.addActionForTask(request.auth.id, request.params.taskId, action).then(function(groups) {
    response.json(groups)
  }, function(error) {
    errorHelper.send(response, error);
  });
});

router.get('/:id/tasks', authorizationHelper.isLoggedIn, function (request, response) {
  taskService.getTasksForToday(request.auth.id).then(function(tasks) {
    response.json(tasks)
  }, function(error) {
    errorHelper.send(response, error);
  });
});


module.exports = router;