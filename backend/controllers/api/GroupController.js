const router = require('express').Router();
const groupService = require('../../services/GroupService');
const errorHelper = require('../../helpers/errorHelper');
const authorizationHelper = require('../../helpers/authorizationHelper');

router.post('/', authorizationHelper.isLoggedIn, (request, response) => {
  let {title, users} = request.body;
  groupService.createGroup(request.auth.id, title, users).then(function(group) {
    response.json(group)
  }, function(error) {
    errorHelper.send(response, error);
  });
});

router.get('/:id', authorizationHelper.isLoggedIn,  (request, response) => {
  groupService.getGroupDetails(request.params.id).then(function(groups) {
    response.json(groups)
  }, function(error) {
    errorHelper.send(response, error);
  });
});

/*router.post('/:id/users', authorizationHelper.isLoggedIn, function (request, response) {
  let {users} = request.body;
  groupService.addUsersToGroup(request.auth.id, request.params.id, users).then(function(group) {
    response.json(group)
  }, function(error) {
    errorHelper.send(response, error);
  });
});*/

router.post('/:id/users/:userId', authorizationHelper.isLoggedIn , (request, response) => {
  groupService.addUserToGroup(request.params.id, request.params.userId, request.auth.id).then(function(group) {
    response.json(group)
  }, function(error) {
    errorHelper.send(response, error);
  });
});

router.put('/:id/users/:userId', authorizationHelper.isLoggedIn, (request, response) => {
  let status;
  if(request.body && request.body.status) {
    status = request.body.status;
  } else {
    errorHelper.send(response, new errorHelper.HttpError(400, "You must provide status."));
  }
  groupService.changeUserStatusInGroup(request.params.id, request.params.userId, request.auth.id, status).then(function(group) {
    response.json(group)
  }, function(error) {
    errorHelper.send(response, error);
  });
});

router.delete('/:id/users/:userId', authorizationHelper.isLoggedIn, function(request, response) {
  groupService.removeUserFromGroup(request.params.id, request.params.userId, request.auth.id).then(function(group) {
    response.json({status: 200, message: "Membership cancelled."})
  }, function(error) {
    errorHelper.send(response, error);
  });
});

module.exports = router;