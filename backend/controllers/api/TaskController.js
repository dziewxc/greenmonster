const router = require('express').Router();
const errorHelper = require('../../helpers/errorHelper');
const authorizationHelper = require('../../helpers/authorizationHelper');
const taskService = require('../../services/TaskService');

router.get('/:id', authorizationHelper.isLoggedIn, function (request, response) {
  taskService.getTaskDetails(request.params.id).then(function(task) {
    response.json(task)
  }, function(error) {
    errorHelper.send(response, error);
  });
});

module.exports = router;