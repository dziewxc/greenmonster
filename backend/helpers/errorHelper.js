const config = require('../config/config');
const log = require('./logHelper');

let HttpError = function (error) {
  this.name = 'HttpError';
  this.status = (error.status === undefined) ? 500 : error.status;
  this.message = error.message;
  this.stack = (new Error()).stack;
};
HttpError.prototype = new Error;

let send = function (response, error) {
  let status = 500;
  let message = error.message;
  if (error instanceof HttpError) {
    status = error.status;
  }
  let errorResponse = {error: true, message: message};
  if (config.debug) {
    errorResponse.stack = error.stack;
  }
  log.log((status >= 500) ? 'ERROR' : 'WARN', message);
  response.status(status).json(errorResponse);
};

module.exports = {send, HttpError};