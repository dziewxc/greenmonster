const models = require('../models');

const testSeed = () => {
  return models.Role.bulkCreate([
    {title: 'user'},
    {title: 'admin'},
  ])
  .then(() => {
    return models.Role.findAll();
  })
  .then((roles) => {
    //TODO should retrieve ids from database
    return models.User.bulkCreate([
      {username: 'User1', password: "password", email: "email1", blocked: "false", role_id: 1},
      {username: 'User2', password: "password", email: "email2", blocked: "false", role_id: 1},
      {username: 'User3', password: "password", email: "email3", blocked: "false", role_id: 1},
      {username: 'User4', password: "password", email: "email4", blocked: "false", role_id: 2},
      {username: 'User5', password: "password", email: "email5", blocked: "true", role_id: 1},
    ])
  })
  .then(() => {
    return models.User.findAll();
  })
  .then((users) => {
    return models.Task.bulkCreate([
      {
        title: 'get your own bag to the shop',
        description: "get your own bag to the shop",
        short_description: "get your own bag to the shop",
        status: "approved",
      },
      {
        title: 'buy milk in glass  bottle',
        description: "buy milk in glass  bottle",
        short_description: "buy milk in glass  bottle",
        status: "approved"
      },
      {
        title: 'reuse something',
        description: "reuse something",
        short_description: "reuse something",
        status: "approved"
      },
      {
        title: 'rejected task',
        description: "rejected task",
        short_description: "rejected task",
        status: "rejected"
      },
      {
        title: 'waiting task',
        description: "waiting task",
        short_description: "waiting task",
        status: "waiting task"
      }
    ])
  })
  .then(() => {
    return models.Task.findAll();
  })
  .then(() => {
    return models.TaskUser.bulkCreate([
      {task_id: 1, user_id: 1, status: "completed"},
      {task_id: 1, user_id: 2, status: "completed"},
      {task_id: 1, user_id: 3, status: "completed"},
      {task_id: 1, user_id: 4, status: "completed"},
    ])
  });
};

module.exports = {testSeed};