let bcrypt = require('bcrypt');
const HttpError = require('../helpers/errorHelper').HttpError;

let bcryptHash = function(password) {
  return new Promise(function(resolve, reject) {
    bcrypt.hash(password, 10, function (error, hash) {
      if (error) {
        return reject(new HttpError(400, error));
      }
      resolve(hash);
    })
  });
};

let bcryptCompare = function(givenPassword, userPassword) {
  return new Promise(function(resolve, reject) {
    bcrypt.compare(givenPassword, userPassword, function (error, valid) {
      if (error) {
        return reject(error);
      }
      if (!valid) {
        return reject(new HttpError(400, "Password is incorrect."));
      }
      resolve(true);
    })
  });
};

module.exports = {bcryptHash, bcryptCompare};