const jwt = require('jwt-simple');
const config = require('../config/config');
const log = require('../helpers/logHelper');

module.exports = function (request, response, next) {
  try {
    if (request.headers['token']) {
      request.auth = jwt.decode(request.headers['token'], config.jwtsecret);
    }
  } catch (error) {
    log.log('ERROR', error.message);
  }
  next();
};