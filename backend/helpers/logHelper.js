const config = require('../config/config');
const statuses = {INFO: 0, WARN: 1, ERROR: 2};

let log = function (status, message) {
  if (statuses[status] >= statuses[config.log]) {
    console.log(status + '[' + (new Date()).toISOString() + ']: ' + message);
  }
};

module.exports = {
  log: log
};