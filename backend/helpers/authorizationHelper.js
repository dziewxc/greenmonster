const errorHelper = require('./errorHelper');

const isLoggedIn = function (request, response, next) {
  if (request.auth) {
    next();
  } else {
    errorHelper.send(response, new errorHelper.HttpError(401, 'Not authorized'));
  }
};

module.exports = {isLoggedIn};