'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('group_user', 'accepted_by_admin', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    })
    .then(function () {
      return queryInterface.renameColumn('group_user', 'accepted', 'accepted_by_user');
    });
  },

  down: function (queryInterface) {
    return queryInterface.removeColumn('group_user', 'accepted_by_admin')
    .then(function () {
      return queryInterface.renameColumn('group_user', 'accepted_by_user', 'accepted');
    });
  }
};
