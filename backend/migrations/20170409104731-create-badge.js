'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('badges', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      description: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      taskAmount: {
        type: Sequelize.INTEGER,
        allowNull: true,
        field: 'task_amount'
      },
      categoryAmount: {
        type: Sequelize.INTEGER,
        allowNull: true,
        field: 'category_amount'
      },
      categoryId: {
        type: Sequelize.INTEGER,
        onDelete: 'SET NULL',
        references: {
          model: 'categories',
          key: 'id'
        },
        field: 'category_id'
      },
      taskId: {
        type: Sequelize.INTEGER,
        onDelete: 'SET NULL',
        references: {
          model: 'tasks',
          key: 'id'
        },
        field: 'task_id'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        field: 'created_at'
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        field: 'updated_at'
      },
      imageId: {
        type: Sequelize.INTEGER,
        onDelete: 'SET NULL',
        references: {
          model: 'images',
          key: 'id'
        },
        field: 'image_id'
      },
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('Badges');
  }
};