'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.changeColumn(
      'group_user',
      'accepted_by_admin',
      {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: "waiting"
      }
    )
  },

  down: function (queryInterface, Sequelize) {
    //
  }
};
