'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.changeColumn(
      'tasks',
      'status',
      {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: "waiting"
      }
    )
  },

  down: function (queryInterface, Sequelize) {
    //
  }
};
