'use strict';
let emitter = require('../services/EventsEmitter');

class featureTaskClass {
  invoke() {
    console.log("got it");
    emitter.emit('complete task');
  }
}

const featureTaskCommand = new featureTaskClass();

module.exports = featureTaskCommand;