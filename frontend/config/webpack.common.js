var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    context: path.join(path.resolve(__dirname, '../')),
    entry: {
      'polyfills': './src/polyfills.ts',
      'vendor': './src/vendor.ts',
      'app': './src/main.ts'
    },
    resolve: {
        extensions: ['*', '.ts', '.js']
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
          name: ['app', 'vendor', 'polyfills']
        }),
        new webpack.ProvidePlugin({
          Reflect: 'core-js/es7/reflect'
        }),
        new webpack.optimize.UglifyJsPlugin({minimize: true}),
        new HtmlWebpackPlugin({
          template: 'index.html'
        }),
    ],
    module: {
      loaders: [
        {
          test: /\.ts$/,
          loaders: ['angular2-template-loader', 'awesome-typescript-loader']
        },
        {
          test: /\.css$/,
          loader: "style!css"
        },
        {
          test: /\.scss/,
          loader: ExtractTextPlugin.extract({
            fallback: "style-loader",
            use: "css-loader"
          })
        },
        { 
          test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.woff2($|\?)|\.eot$|\.ttf$|\.wav$|\.mp3$/,
          loader: "url-loader"
        }
      ]
  }
}