'use strict';

const HtmlWebpack = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const ChunkWebpack = webpack.optimize.CommonsChunkPlugin;
const rootDir = path.resolve(__dirname, '..');

module.exports = {
  devServer: {
    contentBase: path.resolve(rootDir, 'dist'),
    port: 9001
  },
  devtool: 'source-map',
  entry: {
    app: [path.resolve(rootDir, 'src', 'app.js')]
  },
  module: {
      loaders: [
          { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/ },
          { test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/ }
      ]
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(rootDir, 'dist')
  },
  plugins: [
    new HtmlWebpack({
      filename: 'app.html',
      inject: 'body',
      template: path.resolve(rootDir, 'src', 'app.html'),
      debug: true
    })
  ],
  resolve: {
    extensions: ['.js']
  }
};